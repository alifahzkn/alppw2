from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

def home(request):
    return render(request, 'main/home.html')

def sign_up(request):
	if request.method == 'POST':
	    username = request.POST['username']
	    email = request.POST['email']
	    password = request.POST['password']
	    if User.objects.filter(username=username).exists():
	    	messages.error(request, 'Username is not available!')
	    else:
	    	user = User.objects.create_user(username, email, password)
	    	return redirect('/story8')

	return render(request, 'main/signup.html')
