from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def search(request):
	return render(request, 'search.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q="+ request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    data2 = json.loads(ret.content)
    return JsonResponse(data2, safe=False)