from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from .apps import Story7Config
from .views import about
import time

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story7')
		self.assertEqual(apps.get_app_config('story7').name, 'story7')

class Story7UnitTest(TestCase):
	def test_about_url_exists(self):
		response = Client().get('/story7')
		self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
   
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = webdriver.chrome.options.Options()
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

	
		
